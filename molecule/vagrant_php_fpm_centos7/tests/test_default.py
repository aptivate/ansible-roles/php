import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_php_installed(host):
    php_package = host.package('rh-php70')

    assert php_package.is_installed


def test_php_fpm_installed(host):
    php_package = host.package('rh-php70-php-fpm')

    assert php_package.is_installed


def test_php_fpm_is_running(host):
    php_fpm_service = host.service('rh-php70-php-fpm')

    assert php_fpm_service.is_running
    assert php_fpm_service.is_enabled
