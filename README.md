[![pipeline status](https://git.coop/aptivate/ansible-roles/php/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/php/commits/master)

# php

Install and configure PHP.

IMPORTANT: This only works on CentOS 7, and only for version 7.0 of PHP.

It installs php and php-fpm, which enables PHP applications to run under FastCGI.

# Requirements

None

# Role Variables

## With defaults

  * `php_version`: only `"7.0"` works at the moment
    * Defaults to `"7.0"`

# Example Playbook

```yaml
- hosts: localhost
  roles:
    - role: php
      php_version: "7.0"
```

# Testing

For testing the vagrant scenario, you must install the Vagrant executable:

```bash
$ apt-get install vagrant
```

```bash
$ pipenv install --dev
$ pipenv run molecule test -s vagrant_php_fpm_centos7
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
